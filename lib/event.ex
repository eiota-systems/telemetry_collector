defmodule TelemetryCollector.Event do
  @moduledoc """
  """
  use Ecto.Schema

  schema "events" do
    field(:realm, :string)
    field(:tenant, :string)
    field(:generator_id, :string)
    field(:generator_type, :string)
    field(:metric, :string)
    field(:value, :string)
    field(:categories, {:array, :string})
    timestamps()
  end
end
