defmodule TelemetryCollector.Metric do
  @moduledoc """
  """
  use Ecto.Schema

  schema "metrics" do
    field(:realm, :string)
    field(:tenant, :string)
    field(:generator_id, :string)
    field(:generator_type, :string)
    field(:metric, :string)
    field(:value, :float)
    field(:categories, {:array, :string})
    timestamps()
  end
end
