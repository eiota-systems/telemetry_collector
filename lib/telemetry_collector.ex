defmodule TelemetryCollector do
  @moduledoc false
  require Logger

  alias TelemetryCollector.{EnsureMigrations, RealmSupervisor, RealmSync, Repo}
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Caller, Subscriber}

  def start(_, _) do
    topologies = Application.get_env(:telemetry_collector, :topologies)
    quorum = (System.get_env("QUORUM") || "1") |> String.to_integer()
    replicas = (System.get_env("REPLICAS") || "1") |> String.to_integer()
    url = System.get_env("WAMP_SERVER") || "ws://localhost:4000/ws"
    platform_password = System.get_env("PLATFORM_PASSWORD")
    admin_realm = System.get_env("ADMIN_REALM")

    authentication = %Authentication{
      authid: System.get_env("ADMIN_AUTHID"),
      authmethods: ["wampcra"],
      secret: System.get_env("ADMIN_PASSWORD")
    }

    realm = %Realm{name: admin_realm, authentication: authentication}

    admin_roles = [Caller, Subscriber]
    admin_session = %Session{url: url, realm: realm, roles: admin_roles}

    children = [
      Repo,
      EnsureMigrations,
      {ClusterKV,
       name: TelemetryCollectorKV, topologies: topologies, replicas: replicas, quorum: quorum},
      {Task.Supervisor, name: TelemetryCollector.QuerySupervisor},
      {RealmSupervisor, platform_password: platform_password, url: url},
      {Client, name: AdminClient, session: admin_session, reconnect: true},
      {RealmSync, client: AdminClient, admin_realm: admin_realm}
    ]

    opts = [strategy: :one_for_one, name: Telemetry.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
