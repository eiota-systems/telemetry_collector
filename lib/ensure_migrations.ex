defmodule TelemetryCollector.EnsureMigrations do
  @moduledoc false
  require Logger
  use GenServer

  alias TelemetryCollector.{DB, Repo}
  @ecto_repos [Repo]

  @metrics [
    "rosetta.9s8d7f.air_quality.pm",
    "rosetta.9s8d7f.air_quality.co2",
    "rosetta.9s8d7f.air_quality.voc"
  ]

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    ensure_migrations()
    # Process.send_after(self(), :test, 0)
    {:ok, []}
  end

  defp ensure_migrations do
    Logger.info("Ensuring tables have been migrated")

    for repo <- @ecto_repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def handle_info(:test, state) do
    metrics = Enum.map(@metrics, fn m -> [m, Enum.random(10..100)] end)
    t = "org.entropealabs"
    r = "org.entropealabs.clubhouse"

    resp =
      DB.insert_metrics(
        t,
        r,
        "9s8d7f",
        "rosetta",
        metrics,
        ["smart-home", "air-quality"]
      )

    Logger.info("Insert: #{inspect(resp)}")
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    from_date = NaiveDateTime.add(now, -(60 * 60))

    resp = DB.get_metrics(r, @metrics, from_date, now, 5 * 60)

    Logger.info("Get: #{inspect(resp)}")
    Process.send_after(self(), :test, 2000)
    {:noreply, state}
  end
end
