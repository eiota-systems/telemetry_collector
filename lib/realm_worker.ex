defmodule TelemetryCollector.RealmWorker do
  use GenServer
  require Logger

  alias TelemetryCollector.DB
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Callee.{Register, Yield}
  alias Wampex.Roles.Dealer.Invocation
  alias Wampex.Roles.Subscriber.Subscribe

  @get_metrics "telemetry.get_metrics"
  @get_events "telemetry.get_events"

  def start_link(tenant: tenant, realm: realm, client: client, tasks: tasks, agent: agent) do
    GenServer.start_link(__MODULE__, {tenant, realm, client, tasks, agent})
  end

  @impl true
  def init({tenant, realm, client, tasks, agent}) do
    Client.add(client, self())
    register(client)
    {:ok, %{tenant: tenant, realm: realm, client: client, tasks: tasks, agent: agent}}
  end

  @impl true
  def handle_info({:connected, _client}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Event{
          publication_id: pid,
          details: %{"topic" => <<"device.metrics.", _rest::binary()>>},
          arg_list: metrics,
          arg_kw: %{
            "id" => id,
            "tenant" => tenant,
            "categories" => categories,
            "type" => type
          }
        },
        %{realm: realm, tasks: tasks} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      case ClusterKV.update_if(
             TelemetryCollectorKV,
             realm,
             "device.metrics.#{id}",
             pid,
             &update_if/2
           ) do
        :updated ->
          DB.insert_metrics(tenant, realm, id, type, metrics, categories)

        :noop ->
          :noop
      end
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Event{
          publication_id: pid,
          details: %{"topic" => <<"device.events.", _rest::binary()>>},
          arg_list: events,
          arg_kw: %{
            "id" => id,
            "tenant" => tenant,
            "categories" => categories,
            "type" => type
          }
        },
        %{realm: realm, tasks: tasks} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      case ClusterKV.update_if(
             TelemetryCollectorKV,
             realm,
             "device.events.#{id}",
             pid,
             &update_if/2
           ) do
        :updated ->
          DB.insert_events(tenant, realm, id, type, events, categories)

        :noop ->
          :noop
      end
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_metrics},
          arg_kw: %{
            "metrics" => metrics,
            "from_date" => from_date,
            "to_date" => to_date,
            "bucket" => bucket
          }
        },
        %{realm: realm, tasks: tasks, client: client} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      result = DB.get_metrics(realm, metrics, from_date, to_date, bucket)

      result =
        Enum.map(result, fn %{bucket: d} = r ->
          %{r | bucket: NaiveDateTime.to_iso8601(d)}
        end)

      Client.yield(client, %Yield{
        request_id: rid,
        arg_list: result
      })
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{
          request_id: rid,
          details: %{"procedure" => @get_events},
          arg_kw: %{
            "events" => events,
            "from_date" => from_date,
            "to_date" => to_date,
            "bucket" => bucket
          }
        },
        %{realm: realm, client: client, tasks: tasks} = state
      ) do
    Task.Supervisor.start_child(tasks, fn ->
      result = DB.get_events(realm, events, from_date, to_date, bucket)

      result =
        Enum.map(result, fn %{bucket: d} = r ->
          %{r | bucket: NaiveDateTime.to_iso8601(d)}
        end)

      Client.yield(client, %Yield{
        request_id: rid,
        arg_list: result
      })
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(event, state) do
    Logger.info("Realm #{state.realm} received event #{inspect(event)}")
    {:noreply, state}
  end

  defp update_if({key, [old]}, val) do
    case old == val do
      true -> :noop
      false -> {:update, {key, [val]}}
    end
  end

  defp register(client) do
    Logger.info("Registering")
    {:ok, _id} = Client.register(client, %Register{procedure: @get_metrics})
    {:ok, _id} = Client.register(client, %Register{procedure: @get_events})
    Client.subscribe(client, %Subscribe{topic: "device.events"})
    Client.subscribe(client, %Subscribe{topic: "device.metrics"})
  end
end
