defmodule TelemetryCollector.DB do
  @moduledoc false

  require Logger
  alias TelemetryCollector.{Event, Metric, Repo}
  import Ecto.Query, only: [from: 2]

  def get_metrics(realm, metrics, from_date, to_date, bucket) do
    window = %Postgrex.Interval{secs: bucket}

    q =
      from(m in Metric,
        where:
          m.metric in ^metrics and m.realm == ^realm and m.inserted_at > ^from_date and
            m.inserted_at < ^to_date,
        select: %{
          bucket:
            fragment(
              "time_bucket_gapfill((?)::interval, inserted_at) as bucket",
              ^window
            ),
          metric: m.metric,
          count: count() |> coalesce(0),
          percentiles:
            fragment("percentile_cont(?) within group (order by value)", [
              0.25,
              0.5,
              0.75,
              0.9,
              0.95,
              0.99
            ])
            |> coalesce([0, 0, 0, 0, 0, 0]),
          sum: sum(m.value) |> coalesce(0),
          min: min(m.value) |> coalesce(0),
          max: max(m.value) |> coalesce(0),
          avg: avg(m.value) |> coalesce(0)
        },
        group_by: [:metric, fragment("bucket")],
        order_by: [asc: fragment("bucket")]
      )

    Repo.all(q)
  end

  def insert_metrics(tenant, realm, id, type, metrics, categories \\ []) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    metrics = Enum.filter(metrics, fn [_, v] -> is_number(v) end)

    metrics =
      Enum.map(metrics, fn [k, v] ->
        %{
          inserted_at: now,
          updated_at: now,
          realm: realm,
          tenant: tenant,
          generator_id: id,
          generator_type: type,
          categories: categories,
          metric: k,
          value: v / 1
        }
      end)

    Repo.insert_all(Metric, metrics)
  end

  def get_events(realm, events, from_date, to_date, bucket) do
    window = %Postgrex.Interval{secs: bucket}

    q =
      from(m in Event,
        where:
          m.metric in ^events and m.realm == ^realm and m.inserted_at > ^from_date and
            m.inserted_at < ^to_date,
        select: %{
          bucket:
            fragment(
              "time_bucket_gapfill((?)::interval, inserted_at) as bucket",
              ^window
            ),
          metric: m.metric,
          count: count() |> coalesce(0)
        },
        group_by: [:metric, fragment("bucket")],
        order_by: [asc: fragment("bucket")]
      )

    Repo.all(q)
  end

  def insert_events(tenant, realm, id, type, events, categories \\ []) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    events = Enum.filter(events, fn [_, v] -> is_binary(v) end)

    events =
      Enum.map(events, fn [k, v] ->
        %{
          inserted_at: now,
          updated_at: now,
          realm: realm,
          tenant: tenant,
          generator_id: id,
          generator_type: type,
          categories: categories,
          metric: k,
          value: v
        }
      end)

    Repo.insert_all(Event, events)
  end
end
