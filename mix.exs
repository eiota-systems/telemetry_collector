defmodule TelemetryCollector.MixProject do
  use Mix.Project

  def project do
    [
      app: :telemetry_collector,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {TelemetryCollector, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [telemetry_collector: :permanent, runtime_tools: :permanent],
        cookie: "telemetry_cookie"
      ]
    ]
  end

  defp deps do
    [
      {:cluster_kv,
       git: "https://gitlab.com/entropealabs/cluster_kv",
       tag: "e1dfa31b10afba62d513688f21da64c3629ea65c"},
      {:ecto_sql, "~> 3.0"},
      {:postgrex, ">= 0.0.0"},
      {:wampex_client,
       git: "https://gitlab.com/entropealabs/wampex_client.git",
       tag: "93704158ba2fb4db7bb19a454dad3367ecf5d03f"}
    ]
  end
end
