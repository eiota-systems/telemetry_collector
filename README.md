# TelemetryCollector

This application is responsible for persisting all event and metric data that is published from [DeviceShadow](https://gitlab.com/eiota-systems/device_shadow). It also exposes functions to retrieve different aggregations.

The data model consists of two tables in a Timescale database.

The events table

```elixir
schema "events" do
  field(:realm, :string)
  field(:tenant, :string)
  field(:generator_id, :string)
  field(:generator_type, :string)
  field(:metric, :string)
  field(:value, :string)
  field(:categories, {:array, :string})
  timestamps()
end
```

and the metrics table

```elixir
schema "metrics" do
  field(:realm, :string)
  field(:tenant, :string)
  field(:generator_id, :string)
  field(:generator_type, :string)
  field(:metric, :string)
  field(:value, :float)
  field(:categories, {:array, :string})
  timestamps()
end
```

The collector subscribes to the subtopics `device.metrics` and `device.events` in all realms and persists the values to the database.

To retrieve aggregations we call the relevant functions.

A function call would look something like this to retrieve metric data in 5 minute buckets within a time range.

```elixir
Client.call(client, %Call{
  procedure: "telemetry.get_metrics",
  arg_kw: %{
    from_date: NaiveDateTime.to_iso8601(from),
    to_date: NaiveDateTime.to_iso8601(to),
    bucket: 5 * 60,
    metrics: ["rosetta.#{id}.air_quality.co2", "rosetta.#{id}.air_quality.pm"]
  }
})
```

The `metrics` array is used to filter results for a given metric or event type. This is stored based on the `system.type` and `id` from a devices device profile.

