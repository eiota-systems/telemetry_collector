use Mix.Config

alias TelemetryCollector.Repo

config :telemetry_collector, TelemetryCollector.Repo,
  database: "telemetry",
  hostname: "localhost",
  port: 5432,
  username: "postgres",
  password: "postgres"

config :logger,
  level: :info,
  compile_time_purge_matching: [
    [level_lower_than: :info]
  ]

config :telemetry_collector,
  ecto_repos: [Repo],
  topologies: [
    kv: [
      strategy: Cluster.Strategy.Epmd,
      config: [
        hosts: [
          :"app@telemetry-collector1.eiota.systems",
          :"app@telemetry-collector2.eiota.systems",
          :"app@telemetry-collector3.eiota.systems"
        ]
      ],
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]}
    ]
  ]
