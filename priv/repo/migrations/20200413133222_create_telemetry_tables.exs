defmodule TelemetryCollector.Repo.Migrations.CreateTelemetryTables do
  use Ecto.Migration

  def change do
    execute("CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;")

    create table("metrics", primary_key: false) do
      timestamps()
      add(:realm, :string, null: false)
      add(:tenant, :string, null: false)
      add(:generator_id, :string, null: false)
      add(:generator_type, :string, null: false)
      add(:metric, :string, null: false)
      add(:value, :float, null: false)
      add(:categories, {:array, :string})
    end

    execute("SELECT create_hypertable('metrics', 'inserted_at');")

    create table("events", primary_key: false) do
      timestamps()
      add(:realm, :string, null: false)
      add(:tenant, :string, null: false)
      add(:generator_id, :string, null: false)
      add(:generator_type, :string, null: false)
      add(:metric, :string, null: false)
      add(:value, :string, null: false)
      add(:categories, {:array, :string})
    end

    execute("SELECT create_hypertable('events', 'inserted_at');")
  end
end
